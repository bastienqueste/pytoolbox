import pdb
import sys
import numpy as np
import pyqtgraph as pg
from netCDF4 import Dataset
from PyQt5 import QtGui, uic
from PyQt5.QtWidgets import QWidget, QFileDialog

Ui_MainWindow, QtBaseClass = uic.loadUiType("qcApp_gui.ui")

###########################
###########################
##                       ##
##   NETCDF FILE CLASS   ##
##                       ##
###########################
###########################

class EGOData: # EGO standard data format
    # def Get variable names
    def __init__(self,filename):
        self.filename = filename
        self.getVariableList()
        self.getTime()

    def openNetCDF_r(self):
        self.cdfhandle = Dataset(self.filename, mode='r')

    def closeNetCDF(self):
        self.cdfhandle.close()

    def getVariableList(self):
        self.openNetCDF_r()
        self.variableList = list(self.cdfhandle.variables.keys())
        self.propertyList = [variable for variable in self.variableList if variable.__add__("_QC") in self.variableList]
        self.closeNetCDF()

    def getTime(self):
        self.openNetCDF_r()
        self.time = self.cdfhandle.variables['TIME'][:]
        self.time0 = (self.time.data - np.min(self.time.data)) / (np.max(self.time.data) - np.min(self.time.data)) * 1000
        self.closeNetCDF()

    def loadVariables(self, selectedVariable):
        self.openNetCDF_r()

        self.xData = self.cdfhandle.variables[selectedVariable['xVariable']][:]
        self.yData = self.cdfhandle.variables[selectedVariable['yVariable']][:]
        self.cData = self.cdfhandle.variables[selectedVariable['cVariable']][:]

        self.mask = ~self.xData.mask & ~self.yData.mask & ~self.cData.mask # ~self.time.mask # Oddly all the time data seems to be flagged?

        self.closeNetCDF()

#class UWData: # Univ Washington Processing file format
#    def __init__(self,filename):
#        self.filename = filename
#        self.getVariableList()
#        self.getTime()
#
#    def openNetCDF_r(self):
#        self.cdfhandle = Dataset(self.filename, mode='r')
#
#    def closeNetCDF(self):
#        self.cdfhandle.close()
#
#    def getVariableList(self):
#        self.openNetCDF_r()
#        # Add stuff here!
#        # Add stuff here!
#        # Add stuff here!
#        # Add stuff here!
#        self.closeNetCDF()
#
#    def getTime(self):
#        self.openNetCDF_r()
#        self.time0 = (self.time.data - np.min(self.time.data)) / (np.max(self.time.data) - np.min(self.time.data)) * 1000
#        self.closeNetCDF()
#
#    def loadVariables(self, selectedVariable):
#        self.openNetCDF_r()
#
#        self.xData = self.cdfhandle.variables[selectedVariable['xVariable']][:]
#        self.yData = self.cdfhandle.variables[selectedVariable['yVariable']][:]
#        self.cData = self.cdfhandle.variables[selectedVariable['cVariable']][:]
#
#        self.mask = ~self.xData.mask & ~self.yData.mask & ~self.cData.mask # ~self.time.mask # Oddly all the time data seems to be flagged?
#
#        self.closeNetCDF()

###########################
###########################
##                       ##
##     MAIN GUI CLASS    ##
##                       ##
###########################
###########################

class QCApp(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        self.loadData()

        self.invertYAxisCheckBox.stateChanged.connect(self.invertYAxisButton_call)
        self.showLinesCheckBox.stateChanged.connect(self.showLinesButton_call)
        self.largeMarkersCheckBox.stateChanged.connect(self.largeMarkersButton_call)
        
        #self.loadEGODataMenu.triggered.connect(self.loadData)
        #self.loadUWDataMenu.triggered.connect(self.loadData)
        
        ## Initialize callback signals
        self.xVariableBox.currentTextChanged.connect(self.plotSelectedVariable)
        self.yVariableBox.currentTextChanged.connect(self.plotSelectedVariable)
        self.cVariableBox.currentTextChanged.connect(self.plotSelectedVariable)
        self.startSlider.valueChanged.connect(self.plotSelectedVariable)
        self.endSlider.valueChanged.connect(self.plotSelectedVariable)

    ########################################
    ## File loading and parsing functions ##
    ########################################

    ## Loads a file and reads data into memory
    def loadData(self):
        self.loadEGOFile() # Remove later
        self.populateMenus()
        self.plotSelectedVariable()

    def loadEGOFile(self):
        fileSelectDialog = FDApp()
        self.data = EGOData(fileSelectDialog.openFileNameDialog())
        fileSelectDialog.close()
        
    def loadUWFile(self):
        fileSelectDialog = FDApp()
        self.data = UWData(fileSelectDialog.openFileNameDialog())
        fileSelectDialog.close()

    ## Fills menus based on data in memory, or refreshes when settings change
    def populateMenus(self):
        self.xVariableBox.addItems(self.data.propertyList)
        self.yVariableBox.addItems(self.data.propertyList)
        self.cVariableBox.addItems(self.data.variableList)
        # Set lists for each drop down menu
        return

    def plotSelectedVariable(self):
        self.data.loadVariables(dict(
            {'xVariable': self.xVariableBox.currentText(),
             'yVariable': self.yVariableBox.currentText(),
             'cVariable': self.cVariableBox.currentText()}))

        self.plotDataWidget.clear()
        self.histDataWidget.clear()
        
        mask = self.data.mask & (self.data.time0 >= self.startSlider.value()) & (self.data.time0 <= self.endSlider.value())

        self.scatterPlot = self.plotDataWidget.plot(
            self.data.xData[mask],self.data.yData[mask],
            pen=(255,255,255,255),
            symbolBrush=[pg.hsvColor(i) for i in (self.data.cData[mask]-np.min(self.data.cData[mask])) / (np.max(self.data.cData[mask])-np.min(self.data.cData[mask]))]       , #symbolBrush=(0,0,0,130), #
            symbolPen=(0,0,0,200), symbol='o', symbolSize=1)

        y,x = np.histogram(
            self.data.xData[mask], bins=np.linspace(10,14,100))

        self.histPlot = self.histDataWidget.plot(
            x, y,
            stepMode=True, fillLevel=0, brush=(0, 0, 255, 80))


        self.plotDataWidget.autoRange()
        self.histDataWidget.autoRange()

        self.invertYAxisButton_call()
        self.showLinesButton_call()
        self.largeMarkersButton_call()

    #######################################
    ## Currently unused
    #######################################

    ## Define default parameters for use on initial load
    def setInitialConditions(self):
        return

    ## Plots data, or refreshes when settings change
    def populateData(self):
        return

    ## Saves current flag settings in memory (for when changing variables)
    def saveCurrentFlags(self):
        return


    ###################################
    ## Scatter plot graphics options ##
    ###################################

    def invertYAxisButton_call(self):
        if self.invertYAxisCheckBox.isChecked():
            self.plotDataWidget.invertY(True)
        else:
            self.plotDataWidget.invertY(False)

    def showLinesButton_call(self):
        if self.showLinesCheckBox.isChecked():
            self.scatterPlot.setPen(pen=(0,0,0,130))
        else:
            self.scatterPlot.setPen(pen=(255,255,255,255))

    def largeMarkersButton_call(self):
        if self.largeMarkersCheckBox.isChecked():
            self.scatterPlot.setSymbolSize(5)
        else:
            self.scatterPlot.setSymbolSize(1)


#####################################
#####################################
##                                 ##
##     FILE SELECT DIALOG CLASS    ##
##                                 ##
#####################################
#####################################

class FDApp(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'Select EGO format glider timeseries'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        nc_file, _ = QFileDialog.getOpenFileName(self, "Select EGO format glider timeseries", "",
                                                  "NetCDF Files (*.nc);;All Files (*)", options=options)
        return nc_file

#####################################
#####################################
##                                 ##
##              MAIN               ##
##                                 ##
#####################################
#####################################

if __name__ == "__main__":
    ## Set PG broad options once before widget creation
    pg.setConfigOption('background', 'w')
    pg.setConfigOption('foreground', 'k')

    ## Initialise GUIs
    app = QtGui.QApplication(sys.argv)
    mainWindow = QCApp()

    mainWindow.plotDataWidget.showButtons()
    mainWindow.plotDataWidget.showGrid(x=1, y=1, alpha=0.1)

    mainWindow.show()
    sys.exit(app.exec_())
